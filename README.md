# Wide-QWERTY keyboard layout [US]

![Wide-QWERTY Keyboard Layout](wide-qwerty.png)

Colored keys are in non-standard positions 

### Rational:

Keyboards are too cramped.

The pinkies are overused, the thumbs are underused and the capslock key is mostly useless.

This layout aims to improve these problems while moving as few keys as possible from standard QWERTY muscle memory positions.

### Pros:

* Right pinky can reach all the keys more easily.
* Right thumb now has a usable thumb-cluster.
* Thumbs and index fingers dont run in to eachother.
* Enforces using the correct finger for the "b" key (I know you're using your right index, stop it).

### Cons:

* Index fingers need to reach a little for the brackets, curlies and the "7" key.
* On some keyboards (esecially laptops) you may need to learn to use the left thumb for spacebar.
* You will most likely need to re-learn the "b" key since you're probably using the wrong finger (Seriously, cut it out).

### Requirements:

xmodmap, xcape

### How to apply:

xmodmap: `xmodmap .xmod`

xcape: `xcape -e 'Control_L=Escape' -t 10000 `

### Suggestion

If you're a TMUX user, the Home key makes an excellent single press prefix key.

```
# .tmux.conf

set -g prefix Home
bind Home send-prefix
```


